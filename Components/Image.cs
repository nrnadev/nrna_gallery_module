﻿using DotNetNuke.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace NRNA.Modules.GalleryModule.Components
{
    [TableName("GalleryModule_AlbumImage")]
    //setup the primary key for table
    [PrimaryKey("ImageId", AutoIncrement = true)]
    //configure caching using PetaPoco
    [Cacheable("Images", CacheItemPriority.Default, 20)]
    //scope the objects to the ModuleId of a module on a page (or copy of a module on a page)
    //[Scope("PortalId")]
    public class Image
    {
        public int ImageId { get; set; }
        public int AlbumId { get; set; }
        public string ImageFile { get; set; }
        public string Caption { get; set; }
        public bool SetAsAlbumCover { get; set; }
        ///<summary>
        /// The ModuleId of where the object was created and gets displayed
        ///</summary>
        public int ModuleId { get; set; }

        ///<summary>
        /// An integer for the user id of the user who created the object
        ///</summary>
        public int CreatedByUserId { get; set; }

        ///<summary>
        /// An integer for the user id of the user who last updated the object
        ///</summary>
        public int LastModifiedByUserId { get; set; }

        ///<summary>
        /// The date the object was created
        ///</summary>
        public DateTime CreatedOnDate { get; set; }

        ///<summary>
        /// The date the object was updated
        ///</summary>
        public DateTime LastModifiedOnDate { get; set; }
    }
}