﻿/*
' Copyright (c) 2017 Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/
using System.Collections.Generic;
using DotNetNuke.Data;
using System.Text;
using DotNetNuke.Services.FileSystem;
using System;

namespace NRNA.Modules.GalleryModule.Components
{
    class GalleryController
    {
        public void CreateAlbum(Album t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Album>();
                rep.Insert(t);
            }
        }

        public void CreateImage(Image t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                rep.Insert(t);
            }
        }

        public IEnumerable<Album> GetAlbums(int portalId)
        {
            IEnumerable<Album> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<Album>(System.Data.CommandType.Text, @"SELECT * FROM GalleryModule_Album WHERE PortalId=@0", portalId);
            }
            //using (IDataContext ctx = DataContext.Instance())
            //{
            //    var rep = ctx.GetRepository<Album>();
            //    t = rep.Get(moduleId);
            //}
            return t;
        }

        public IEnumerable<Album> GetTop4Albums(int portalId)
        {
            IEnumerable<Album> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<Album>(System.Data.CommandType.Text, @"SELECT TOP 4 * FROM GalleryModule_Album WHERE PortalId=@0", portalId);
            }
            //using (IDataContext ctx = DataContext.Instance())
            //{
            //    var rep = ctx.GetRepository<Album>();
            //    t = rep.Get(moduleId);
            //}
            return t;
        }

        public Album GetGallery(int albumId, int portalId)
        {
            Album t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Album>();
                t = rep.GetById(albumId, portalId);
            }
            return t;
        }

        public void DeleteGallery(int albumId, int portalid)
        {
            var t = GetGallery(albumId, portalid);
            DeleteAlbum(t);
        }

        public void DeleteAlbum(Album t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Album>();
                rep.Delete(t);
            }
        }

        public void DeleteAllImage(Image t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                rep.Delete(t);
            }
        }

        public IEnumerable<Image> GetAlbumImages(int moduleId)
        {
            IEnumerable<Image> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                t = rep.Get(moduleId);
            }
            return t;
        }

        public Album GetAlbum(int itemId, int portalId)
        {
            Album t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Album>();
                t = rep.GetById(itemId, portalId);
            }
            return t;
        }


        public Image GetImage(int itemId)
        {
            Image t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                t = rep.GetById(itemId);
            }
            return t;
        }

        public Image GetImageByAlbumId(int itemId)
        {
            Image t;
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                t = rep.GetById(itemId);
            }
            return t;
        }

        public void UpdateAlbum(Album t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Album>();
                rep.Update(t);
            }
        }

        public void UpdateImage(Image t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                rep.Update(t);
            }
        }

        public string GetAlbumLink(int tabId, int AlbumId)
        {
            return DotNetNuke.Common.Globals.NavigateURL(tabId, String.Empty, "albid=" + AlbumId);
        }

        public IEnumerable<Image> GetAllAlbumImages(int albumid, int moduleId)
        {
            IEnumerable<Image> t;

            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                t = rep.Find("WHERE AlbumId=@0 AND ModuleId=@1", albumid, moduleId);
            }
            return t;
        }
        
        public string GetAlbumCover(int albumid)
        {
            IEnumerable<Image> t;
            string fileId = "";

            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                t = rep.Find("WHERE AlbumId=@0 AND SetAsAlbumCover=1", albumid);

            }
            foreach (var st in t)
            {
                fileId = st.ImageFile;
                break;
            }
            if (fileId == "")
            {
                using (IDataContext ctx = DataContext.Instance())
                {
                    var rep = ctx.GetRepository<Image>();
                    t = rep.Find("WHERE AlbumId=@0", albumid);
                }
                foreach (var st in t)
                {
                    fileId = st.ImageFile;
                    break;
                }
            }
            if (fileId == "-1")
            {
                fileId = "3245";
            }
            StringBuilder sb = new StringBuilder("/Portals/");
            try
            {
                IFileInfo fi = FileManager.Instance.GetFile(Convert.ToInt32(fileId));
                sb.Append(fi.PortalId);
                sb.Append("/");
                sb.Append(fi.RelativePath);
            }
            catch (Exception ex)
            {
                return "";
            }
            return sb.ToString();
        }

        public void DeleteImage(int itemId, int moduleId)
        {
            var t = GetImage(itemId);
            DeleteImage(t);
        }

        public void DeleteImage(Image t)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                var rep = ctx.GetRepository<Image>();
                rep.Delete(t);
            }
        }

        public IEnumerable<Image> UpdateAlbumCover(int albumid)
        {
            IEnumerable<Image> t;
            using (IDataContext ctx = DataContext.Instance())
            {
                t = ctx.ExecuteQuery<Image>(System.Data.CommandType.Text, @"UPDATE GalleryModule_AlbumImage SET SetAsAlbumCover=0 WHERE AlbumId=@0", albumid);
            }
            return t;
        }

        public void DeleteImageByAlbumId(int albumid, int moduleId)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                ctx.ExecuteQuery<Image>(System.Data.CommandType.Text, @"DELETE FROM GalleryModule_AlbumImage WHERE AlbumId=@0 AND ModuleId=@1", albumid, moduleId);
            }
        }

        public void DeleteAlbum(int albumid, int moduleId)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                ctx.ExecuteQuery<Image>(System.Data.CommandType.Text, @"DELETE FROM GalleryModule_Album WHERE AlbumId=@0 AND ModuleId=@1", albumid, moduleId);
            }
        }

        public void DeleteAllImage(int albumid)
        {
            using (IDataContext ctx = DataContext.Instance())
            {
                ctx.ExecuteQuery<Image>(System.Data.CommandType.Text, @"DELETE FROM [GalleryModule_AlbumImage] WHERE AlbumId=@0", albumid);
            }
        }

    }
}
