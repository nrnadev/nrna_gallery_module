﻿using DotNetNuke.UI.Utilities;
using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class AlbumListView : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var tc = new GalleryController();
                rptrItem.DataSource = tc.GetAlbums(PortalId);
                rptrItem.DataBind();
            }
        }

        protected void rptItemListOnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                //var lnkEdit = e.Item.FindControl("lnkEdit") as HyperLink;
                var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

                var pnlAdminControls = e.Item.FindControl("pnlAdmin") as Panel;

                var t = (Components.Album)e.Item.DataItem;

                if (IsEditable && lnkDelete != null && pnlAdminControls != null)
                {
                    pnlAdminControls.Visible = true;
                    lnkDelete.CommandArgument = t.AlbumId.ToString();
                    lnkDelete.Enabled = lnkDelete.Visible = true;

                    //lnkEdit.NavigateUrl = EditUrl(string.Empty, string.Empty, "Edit", "aid=" + t.ImageId);

                    ClientAPI.AddButtonConfirm(lnkDelete, "Are you sure you want to delete?");
                }
                else
                {
                    pnlAdminControls.Visible = false;
                }
            }
        }

        public void rptItemListOnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "Edit")
            //{
            //    Response.Redirect(EditUrl(string.Empty, string.Empty, "Edit", "imgid=" + e.CommandArgument));
            //}

            if (e.CommandName == "Delete")
            {
                var tc = new GalleryController();
                tc.DeleteImageByAlbumId(Convert.ToInt32(e.CommandArgument), PortalId);
                tc.DeleteAlbum(Convert.ToInt32(e.CommandArgument), PortalId);
            }
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }
    }
}