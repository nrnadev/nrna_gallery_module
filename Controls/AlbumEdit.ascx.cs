﻿using DotNetNuke.Services.Exceptions;
using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class AlbumEdit : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Implement your edit logic for your module
                if (!Page.IsPostBack)
                {
                    if (AlbumId > 0)
                    {
                        var tc = new GalleryController();

                        var t = tc.GetAlbum(AlbumId, PortalId);
                        if (t != null)
                        {
                            txtAlbumName.Text = t.AlbumTitle;
                        }
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        protected void btnSaveImage_Click(object sender, EventArgs e)
        {
            try
            {
                var t = new Album();
                var tc = new GalleryController();

                if (AlbumId > 0)
                {
                    t = tc.GetGallery(AlbumId, PortalId);
                }
                t.AlbumTitle = txtAlbumName.Text.Trim();
                if (t.AlbumId > 0)
                {
                    tc.UpdateAlbum(t);
                }
            }
            catch (Exception exc)
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }
    }
}