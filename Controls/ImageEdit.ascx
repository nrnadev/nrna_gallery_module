﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageEdit.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.ImageEdit" %>

<h3>
    <asp:Label runat="server" ID="lblTitle"></asp:Label></h3>

<div class="photo-gallery">
    <asp:Repeater runat="server" ID="rptrItem" OnItemDataBound="rptItemListOnItemDataBound" OnItemCommand="rptItemListOnItemCommand">
        <ItemTemplate>
            <div class="gallery">
                <div class="image">
                    <asp:HyperLink ID="HyperLink1" runat="server"
                        NavigateUrl='<%# GetAlbumImageFile(DataBinder.Eval(Container.DataItem,"ImageFile").ToString())%>' CssClass="lightbox" rel="gallery">
                    
                    <asp:Image runat="server"
                        ImageUrl='<%# GetImageFromImageID(DataBinder.Eval(Container.DataItem,"ImageFile").ToString()) %>' AlternateText="image"></asp:Image></asp:HyperLink>
                </div>
                <strong class="title">
                    <asp:Label ID="lblCaption" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Caption").ToString() %>'>
                    </asp:Label>
                </strong>
                <asp:Panel ID="pnlAdmin" runat="server">
                    <asp:HyperLink ID="lnkEdit" runat="server" NavigateUrl='<%# "~/edit-image/imgid/" + DataBinder.Eval(Container.DataItem,"ImageId").ToString() %>'
                        Text="Edit" CommandName="Edit"></asp:HyperLink>
                    <asp:LinkButton ID="lnkDelete" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ImageId").ToString() %>' Text="Delete" CommandName="Delete"></asp:LinkButton>
                </asp:Panel>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
