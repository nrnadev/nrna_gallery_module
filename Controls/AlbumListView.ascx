﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlbumListView.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.AlbumListView" %>
<br />
<div class="photo-gallery">
    <asp:Repeater runat="server" ID="rptrItem">
        <ItemTemplate>
            <div class="gallery album">
                <div class="image">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# GetAlbumLink(DataBinder.Eval(Container.DataItem,"AlbumId").ToString())%>'>
                        <asp:Image runat="server" ImageUrl='<%# GetAlbumCover(DataBinder.Eval(Container.DataItem,"AlbumId").ToString()) %>'
                            style="max-height:155px;" ></asp:Image>
                    </asp:HyperLink>

                </div>
                <strong class="title">
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# GetAlbumLink(DataBinder.Eval(Container.DataItem,"AlbumId").ToString())%>'
                        Text='<%# DataBinder.Eval(Container.DataItem,"AlbumTitle").ToString() %>'>
                    </asp:HyperLink>
                </strong>
                <%--<asp:Panel ID="pnlAdmin" runat="server" Visible="false">
                    <asp:HyperLink ID="lnkEdit" runat="server" Text="Edit" Visible="false" Enabled="false" CommandName="Edit"></asp:HyperLink>
                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" Visible="false" Enabled="false" CommandName="Delete"></asp:LinkButton>
                </asp:Panel>--%>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
