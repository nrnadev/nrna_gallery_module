﻿using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class EditGalleryImage : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                albumCover.Visible = false;
                if (!Page.IsPostBack)
                {
                    BindDropDownList();
                    if (ddlAlbums.Items.Count > 0)
                    {
                        pnlimageUploadForm.Visible = true;
                    }
                    if (ImageId > 0)
                    {
                        hdnAlbumId.Visible = false;
                        albumCover.Visible = true;
                        var tc = new GalleryController();
                        var t = tc.GetImage(ImageId);
                        if (t != null)
                        {
                            ddlAlbums.SelectedValue = t.AlbumId.ToString();

                            pnlImage1.Visible = true;
                            pnlImage.Visible = false;
                            //pnlImage3.Visible = false;
                            //pnlImage4.Visible = false;
                            //pnlImage5.Visible = false;
                            fileuploader1.FileID = Convert.ToInt32(t.ImageFile);
                            txtCaption1.Text = t.Caption;
                            chksetascover.Checked = Convert.ToBoolean(t.SetAsAlbumCover);
                        }
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                //Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        public void BindDropDownList()
        {
            var tc = new GalleryController();

            ddlAlbums.DataSource = (List<Album>)tc.GetAlbums(PortalId);
            ddlAlbums.DataTextField = "AlbumTitle";
            ddlAlbums.DataValueField = "AlbumId";
            ddlAlbums.DataBind();
        }

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            var t = new Album();
            var tc = new GalleryController();
            if (AlbumId > 0)
            {
                t = tc.GetAlbum(AlbumId, PortalId);
            }
            else
            {
                t.CreatedByUserId = UserId;
                t.CreatedOnDate = DateTime.Now;
            }
            t.AlbumTitle = txtAlbumName.Text;
            t.AlbumCreatedFromDomain = PortalAlias.HTTPAlias;
            t.LastModifiedByUserId = UserId;
            t.LastModifiedOnDate = DateTime.Now;
            t.ModuleId = ModuleDefinitionID;
            t.Country = PortalSettings.PortalName;
            t.PostedFromDomain = PortalAlias.HTTPAlias;
            t.PortalId = PortalId;
            t.PermanentURL = PortalAlias.HTTPAlias + "/tabid/" + TabId;

            tc.CreateAlbum(t);
            
            BindDropDownList();
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        protected void btnSaveImage_Click(object sender, EventArgs e)
        {
            var t = new Components.Image();
            var tc = new GalleryController();

            if (ImageId > 0)
            {
                t = tc.GetImage(ImageId);
            }
            else
            {
                t.CreatedByUserId = UserId;
                t.CreatedOnDate = DateTime.Now;
            }

            t.AlbumId = Convert.ToInt32(ddlAlbums.SelectedItem.Value.ToString());
            
            t.SetAsAlbumCover = false;
            t.LastModifiedByUserId = UserId;
            t.LastModifiedOnDate = DateTime.Now;
            t.ModuleId = ModuleDefinitionID;
            if (chksetascover.Checked)
            {
                tc.UpdateAlbumCover(t.AlbumId);
                t.SetAsAlbumCover = true;
            }
            else
            {
                t.SetAsAlbumCover = false;
            }



            if (ImageId > 0)
            {
                t.ImageFile = fileuploader1.FileID.ToString();
                t.Caption = txtCaption1.Text.Trim();

                tc.UpdateImage(t);
            }
            else
            {
                if (fileuploader1.FileID.ToString() != "-1")
                {
                    t.ImageFile = fileuploader1.FileID.ToString();
                    t.Caption = txtCaption1.Text.Trim();
                    tc.CreateImage(t);
                }
                if (fileuploader2.FileID.ToString() != "-1")
                {
                    t.ImageFile = fileuploader2.FileID.ToString();
                    t.Caption = txtCaption2.Text.Trim();
                    tc.CreateImage(t);
                }
                if (fileuploader3.FileID.ToString() != "-1")
                {
                    t.ImageFile = fileuploader3.FileID.ToString();
                    t.Caption = txtCaption3.Text.Trim();
                    tc.CreateImage(t);
                }
                if (fileuploader4.FileID.ToString() != "-1")
                {
                    t.ImageFile = fileuploader4.FileID.ToString();
                    t.Caption = txtCaption4.Text.Trim();
                    tc.CreateImage(t);
                }
                if (fileuploader5.FileID.ToString() != "-1")
                {
                    t.ImageFile = fileuploader5.FileID.ToString();
                    t.Caption = txtCaption5.Text.Trim();
                    tc.CreateImage(t);
                }
            }
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        }
    }
}