﻿using DotNetNuke.Services.Exceptions;
using DotNetNuke.UI.Utilities;
using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class ManageGallery : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                bindGV();
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        private void bindGV()
        {
            var tc = new GalleryController();

            gv.DataSource = tc.GetAlbums(PortalId);
            gv.DataBind();
        }

        protected void gv_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lnkDelete = e.Row.FindControl("lnkDelete") as LinkButton;
                var t = (Album)e.Row.DataItem;

                lnkDelete.CommandArgument = t.AlbumId.ToString();
                lnkDelete.Enabled = lnkDelete.Visible = true;

                ClientAPI.AddButtonConfirm(lnkDelete, "All the Images in this Gallery will be deleted. Are you sure you want to delete?");
            }
        }

        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                var tc = new GalleryController();
                tc.DeleteAllImage(Convert.ToInt32(e.CommandArgument));
                tc.DeleteGallery(Convert.ToInt32(e.CommandArgument), PortalId);
            }
        }

        protected void gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            bindGV();
        }

        protected void gv_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv.PageIndex = e.NewPageIndex;
            bindGV();
        }
    }
}