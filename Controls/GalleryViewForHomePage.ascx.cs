﻿using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class GalleryViewForHomePage : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var tc = new GalleryController();
                    rptItemList.DataSource = tc.GetTop4Albums(PortalId);
                    rptItemList.DataBind();
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}