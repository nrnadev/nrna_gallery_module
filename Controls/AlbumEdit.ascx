﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlbumEdit.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.AlbumEdit" %>
<h2>Edit Album</h2>
<table>
    <tr>
        <%-- <input type="button" id="btnAddNewAlbum" onclick="showTextBox()" value="Add New Album" />--%>

        <%--<dnn:label id="lblAlbumName" text="Album Title" runat="server" />--%>
        <td style="vertical-align: top; width: 200px;">Album Name:
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtAlbumName" data-id="albumName" Text=""></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:LinkButton runat="server" CssClass="dnnPrimaryAction"
                ID="btnSaveImage" OnClick="btnSaveImage_Click" Text="Save"></asp:LinkButton>
            <asp:LinkButton runat="server" CssClass="dnnSecondaryAction"
                ID="btnCancel" OnClick="btnCancel_Click" Text="Cancel"></asp:LinkButton>
        </td>
    </tr>
</table>