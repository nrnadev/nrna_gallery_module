﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageGallery.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.ManageGallery" %>
<h2>Manage Gallery</h2>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.UI.WebControls" Assembly="DotNetNuke" %>
<asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-blue" Text="Add Gallery/Image" Visible="true" Enabled="true"
    NavigateUrl="~/add-image" Target="_blank" />
<br />
<br />
<div class="table-responsive">
    <asp:GridView runat="server" ID="gv" AllowCustomPaging="False" AllowPaging="True" OnPageIndexChanging="gv_PageIndexChanging"
        PageSize="10" DataKeyNames="AlbumId" AutoGenerateColumns="False" OnRowDataBound="gv_RowDataBound"
        OnRowCommand="gv_RowCommand" OnRowDeleting="gv_RowDeleting" CssClass="table">
        <Columns>
            <asp:BoundField DataField="AlbumId" HeaderText="Album Id"></asp:BoundField>
            <asp:TemplateField HeaderText="ThumbNail Image">
                <ItemTemplate>
                    <div class="img-holder">
                        <asp:Image runat="server" Style="max-width: 120px; max-height: 120px" ImageUrl='<%# GetAlbumCover(DataBinder.Eval(Container.DataItem,"AlbumId").ToString()) %>'
                            AlternateText="Click on image to view original image" />
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="AlbumTitle" HeaderText="Album Title" ItemStyle-Width="35%">
            </asp:BoundField>
            <asp:TemplateField HeaderText="Manage" ItemStyle-Width="31%">
                <ItemTemplate>
                    <asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-green" Text="Edit Album"
                        Visible="true" Enabled="true" Target="_blank" NavigateUrl='<%# "~/edit-album/albid/" + DataBinder.Eval(Container.DataItem,"AlbumId").ToString() %>' />
                    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button button-green" Text="Edit Image"
                        Visible="true" Enabled="true" Target="_blank" NavigateUrl='<%# "~/edit-image/albid/" + DataBinder.Eval(Container.DataItem,"AlbumId").ToString() %>' />
                    <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button  button-donate" Text="Delete"
                        Visible="true" Enabled="true" CommandName="Delete" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>


<%--<asp:GridView runat="server" ID="gv" AllowCustomPaging="False" AllowPaging="True" PageSize="10" DataKeyNames="AlbumId"
    AutoGenerateColumns="False" OnRowDataBound="gv_RowDataBound" OnRowCommand="gv_RowCommand" OnRowDeleting="gv_RowDeleting">
    <Columns>
        <asp:BoundField DataField="AlbumId" HeaderText="Album Id"></asp:BoundField>
         <asp:TemplateField HeaderText="ThumbNail Image">
            <ItemTemplate>
                <asp:Image runat="server" Style="max-width: 120px; max-height: 120px" ImageUrl='<%# GetAlbumCover(DataBinder.Eval(Container.DataItem,"AlbumId").ToString()) %>'
                    AlternateText="Click on image to view original image" />
            </ItemTemplate>
        </asp:TemplateField>

        <asp:BoundField DataField="AlbumTitle" HeaderText="Album Title"></asp:BoundField>
       
        <asp:TemplateField HeaderText="Manage">
            <ItemTemplate>
                <asp:HyperLink ID="lnkEdit" runat="server" CssClass="button button-green" Text="Edit Album" Visible="true" Enabled="true"
                    Target="_blank" NavigateUrl='<%# "~/edit-album/albid/" + DataBinder.Eval(Container.DataItem,"AlbumId").ToString() %>' />
                <br />
                <br />
                 <asp:HyperLink ID="HyperLink1" runat="server" CssClass="button button-green" Text="Edit Image" Visible="true" Enabled="true"
                    Target="_blank" NavigateUrl='<%# "~/edit-image/albid/" + DataBinder.Eval(Container.DataItem,"AlbumId").ToString() %>' />
                <br />
                <br />
                <asp:LinkButton ID="lnkDelete" runat="server" CssClass="button  button-donate" Text="Delete" Visible="true" Enabled="true" CommandName="Delete" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>--%>