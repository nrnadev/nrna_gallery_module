﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GalleryViewForHomePage.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.GalleryViewForHomePage" %>
<div class="skin position-r screen-mrg-t">
    <div class="heading-mini heading-mini-1">
        <h2 class="text">Gallery</h2>
    </div>
    <asp:Repeater ID="rptItemList" runat="server">
        <HeaderTemplate>
            <ul class="compact-gridlist gallery-home">
        </HeaderTemplate>
        <ItemTemplate>

            <li class="compact-gridlist-item">
                 <a runat="server" NavigateUrl='<%# GetAlbumLink(DataBinder.Eval(Container.DataItem,"AlbumId").ToString())%>'>
                    <img src='<%# GetAlbumCover(DataBinder.Eval(Container.DataItem,"AlbumId").ToString()) %>' alt="click to view original Image">
                    <i class="fa fa-image"></i>
                </a>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>      
        </FooterTemplate>
    </asp:Repeater>
    <div class="pdd-xs-all txt-center"><a href="~/Gallery" class="link">View all</a></div>
</div>
