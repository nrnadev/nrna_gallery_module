﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditGalleryImage.ascx.cs" Inherits="NRNA.Modules.GalleryModule.Controls.EditGalleryImage" %>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TextEditor" Src="~/controls/TextEditor.ascx" %>
<%@ Register TagPrefix="dnn" TagName="FilePickerUploader" Src="~/controls/FilePickerUploader.ascx" %>


<div class="dnnForm dnnEditBasicSettings" id="dnnEditBasicSettings">
    <h2>Add/Edit Image</h2>

    <fieldset>
        <asp:HiddenField runat="server" ID="hdnAlbumId" />
        <input type="button" id="btnAddNewAlbum" onclick="showTextBox()" value="Add New Album" />
        <div id="newAlbumName" class="dnnFormItem" style="display: none;">
            <dnn:Label ID="lblAlbumName" Text="Album Title" runat="server" />
            <asp:TextBox runat="server" ID="txtAlbumName" data-id="albumName" Text=""></asp:TextBox>
            <asp:LinkButton runat="server" CssClass="dnnPrimaryAction" ID="btnSubmit" OnClick="btnSubmit_Click" Text="Save"></asp:LinkButton>
        </div>

    </fieldset>
    <div class="table-responsive">
        <asp:Panel runat="server" ID="pnlimageUploadForm" Visible="false">
            <table class="table">
                <tr>
                    <td style="vertical-align: top; width: 200px;">Choose Album:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlAlbums"></asp:DropDownList>
                    </td>
                </tr>
                <asp:Panel runat="server" ID="pnlImage1">
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Upload Thumbnail 1 (150px X 150px):
                        </td>
                        <td>
                            <dnn:FilePickerUploader ID="fileuploader1" FileFilter="jpg,png,gif,jpeg" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Caption 1:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCaption1"></asp:TextBox>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlImage">

                    <tr>
                        <td style="vertical-align: top; width: 200px;">Upload Thumbnail 2 (150px X 150px):
                        </td>
                        <td>
                            <dnn:FilePickerUploader ID="fileuploader2" FileFilter="jpg,png,gif,jpeg" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Caption 2:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCaption2"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Upload Thumbnail 3 (150px X 150px):
                        </td>
                        <td>
                            <dnn:FilePickerUploader ID="fileuploader3" FileFilter="jpg,png,gif,jpeg" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Caption 3:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCaption3"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Upload Thumbnail 4 (150px X 150px):
                        </td>
                        <td>
                            <dnn:FilePickerUploader ID="fileuploader4" FileFilter="jpg,png,gif,jpeg" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Caption 4:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCaption4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Upload Thumbnail 5 (150px X 150px):
                        </td>
                        <td>
                            <dnn:FilePickerUploader ID="fileuploader5" FileFilter="jpg,png,gif,jpeg" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Caption 5:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCaption5"></asp:TextBox>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="albumCover" runat="server">
                    <tr>
                        <td style="vertical-align: top; width: 200px;">Set As Cover:
                        </td>
                        <td>
                            <asp:CheckBox ID="chksetascover" runat="server" />
                        </td>
                    </tr>
                </asp:Panel>
                <tr>
                    <td colspan="2">
                        <asp:LinkButton runat="server" CssClass="dnnPrimaryAction"
                            ID="btnSaveImage" OnClick="btnSaveImage_Click" Text="Save Image"></asp:LinkButton>
                        <asp:LinkButton runat="server" CssClass="dnnSecondaryAction"
                            ID="btnCancel" OnClick="btnCancel_Click" Text="Cancel"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</div>


<script type="text/javascript">
    function showTextBox() {
        $('#newAlbumName').css('display', 'block');
        //$("#newAlbumName :input").val('');
        return false;
        F
    }
    /*globals jQuery, window, Sys */
    (function ($, Sys) {
        function dnnEditBasicSettings() {
            $('#dnnEditBasicSettings').dnnPanels();
            $('#dnnEditBasicSettings .dnnFormExpandContent a').dnnExpandAll({ expandText: '<%=Localization.GetString("ExpandAll", LocalResourceFile)%>', collapseText: '<%=Localization.GetString("CollapseAll", LocalResourceFile)%>', targetArea: '#dnnEditBasicSettings' });
        }

        $(document).ready(function () {
            dnnEditBasicSettings();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                dnnEditBasicSettings();
            });

          <%-- $("#<%=txtPressReleaseDate.ClientID %>").datepicker();--%>


        });



    }(jQuery, window.Sys));
</script>
