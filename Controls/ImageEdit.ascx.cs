﻿using DotNetNuke.UI.Utilities;
using NRNA.Modules.GalleryModule.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NRNA.Modules.GalleryModule.Controls
{
    public partial class ImageEdit : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AlbumId > 0)
            {
                var tc = new GalleryController();
                rptrItem.DataSource = tc.GetAllAlbumImages(AlbumId, ModuleDefinitionID);
                rptrItem.DataBind();
                var td = new Album();
                var t = tc.GetAlbum(AlbumId, PortalId);
                lblTitle.Text = t.AlbumTitle;
            }
        }

        protected void rptItemListOnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {  
                var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

                var pnlAdminControls = e.Item.FindControl("pnlAdmin") as Panel;

                var t = (GalleryModule.Components.Image)e.Item.DataItem;

                if (lnkDelete != null && pnlAdminControls != null)
                {
                    pnlAdminControls.Visible = true;
                    lnkDelete.CommandArgument = t.ImageId.ToString();
                    //lnkDelete.Enabled = lnkDelete.Visible = lnkEdit.Enabled = lnkEdit.Visible = true;

                    //lnkEdit.NavigateUrl = EditUrl(string.Empty, string.Empty, "Edit", "imgid=" + t.ImageId);

                    ClientAPI.AddButtonConfirm(lnkDelete, "Are you sure you want to delete?");
                }
                //else
                //{
                //    pnlAdminControls.Visible = false;
                //}
            }
        }

        public void rptItemListOnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            //if (e.CommandName == "Edit")
            //{
            //    Response.Redirect(EditUrl(string.Empty, string.Empty, "Edit", "imgid=" + e.CommandArgument));
            //    Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
            //}

            if (e.CommandName == "Delete")
            {
                var tc = new GalleryController();
                tc.DeleteImage(Convert.ToInt32(e.CommandArgument), ModuleDefinitionID);
                Response.Redirect(DotNetNuke.Common.Globals.NavigateURL(string.Empty,"albid="+ AlbumId));
            }
        }
    }
}