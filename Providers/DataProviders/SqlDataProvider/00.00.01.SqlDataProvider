﻿/************************************************************/
/*****              SqlDataProvider                     *****/
/*****                                                  *****/
/*****                                                  *****/
/***** Note: To manually execute this script you must   *****/
/*****       perform a search and replace operation     *****/
/*****       for {databaseOwner} and {objectQualifier}  *****/
/*****                                                  *****/
/************************************************************/

IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'{databaseOwner}[{objectQualifier}GalleryModule_AlbumImage]') AND type in (N'U'))
	DROP TABLE {databaseOwner}[{objectQualifier}GalleryModule_AlbumImage]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE [object_id] = OBJECT_ID(N'{databaseOwner}[{objectQualifier}GalleryModule_Album]') AND type in (N'U'))
	DROP TABLE {databaseOwner}[{objectQualifier}GalleryModule_Album]
GO

CREATE TABLE {databaseOwner}{objectQualifier}GalleryModule_Album
(
	[AlbumId] INT NOT NULL IDENTITY (1, 1),
	[AlbumTitle] NVARCHAR(MAX) NOT NULL,
	[AlbumCreatedFromDomain] NVARCHAR(128) NOT NULL,
	[ModuleId] INT NOT NULL,
	[Country] NVARCHAR(32) NOT NULL,
	[PostedFromDomain] NVARCHAR(128) NOT NULL,
	[PortalId] INT NOT NULL,
	[PermanentURL] NVARCHAR(256) NOT NULL,
	[CreatedOnDate] DATETIME NOT NULL,
	[CreatedByUserId] INT NOT NULL,
	[LastModifiedOnDate] DATETIME NOT NULL,
	[LastModifiedByUserId] INT NOT NULL
)
GO


ALTER TABLE {databaseOwner}[{objectQualifier}GalleryModule_Album] ADD CONSTRAINT [PK_{objectQualifier}GalleryModule_Album] PRIMARY KEY CLUSTERED ([AlbumId])
GO

CREATE TABLE {databaseOwner}{objectQualifier}GalleryModule_AlbumImage
(
	[ImageId] INT NOT NULL IDENTITY (1, 1),
	[AlbumId] INT NOT NULL,
	[ImageFile] NVARCHAR(256) NOT NULL,
	[Caption] NVARCHAR(2000) NOT NULL,
	[SetAsAlbumCover] BIT NOT NULL,
	[ModuleId] INT NOT NULL,
	[CreatedOnDate] DATETIME NOT NULL,
	[CreatedByUserId] INT NOT NULL,
	[LastModifiedOnDate] DATETIME NOT NULL,
	[LastModifiedByUserId] INT NOT NULL
)
GO


ALTER TABLE {databaseOwner}[{objectQualifier}GalleryModule_AlbumImage] ADD CONSTRAINT [PK_{objectQualifier}GalleryModule_AlbumImage] PRIMARY KEY CLUSTERED ([ImageId])
GO

ALTER TABLE {databaseOwner}[{objectQualifier}GalleryModule_AlbumImage] ADD CONSTRAINT [FK_{objectQualifier}GalleryModule_AlbumImage]
FOREIGN KEY ([AlbumId]) REFERENCES {databaseOwner}[{objectQualifier}GalleryModule_Album](AlbumId)

GO


/************************************************************/
/*****              SqlDataProvider                     *****/
/************************************************************/