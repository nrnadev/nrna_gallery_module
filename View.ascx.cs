﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Web.UI.WebControls;
using NRNA.Modules.GalleryModule.Components;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.UI.Utilities;

namespace NRNA.Modules.GalleryModule
{
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The View class displays the content
    /// 
    /// Typically your view control would be used to display content or functionality in your module.
    /// 
    /// View may be the only control you have in your project depending on the complexity of your module
    /// 
    /// Because the control inherits from GalleryModuleModuleBase you have access to any custom properties
    /// defined there, as well as properties from DNN such as PortalId, ModuleId, TabId, UserId and many more.
    /// 
    /// </summary>
    /// -----------------------------------------------------------------------------
    public partial class View : GalleryModuleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var controlToLoad = "";

                if (Settings.Contains("DefaultView"))
                {
                    var defaultview = Settings["DefaultView"].ToString();
                    switch (defaultview)
                    {
                        case "0":
                            controlToLoad = "Controls/AlbumListView.ascx";
                            if (AlbumId > 0)
                            {
                                controlToLoad = "Controls/AlbumImagesView.ascx";
                            }
                            break;
                        case "1":
                            controlToLoad = "Controls/ManageGallery.ascx";
                            if (AlbumId > 0)
                            {
                                controlToLoad = "Controls/ImageEdit.ascx";
                            }
                            break;
                        case "2": controlToLoad = "Controls/EditGalleryImage.ascx"; break;
                        case "3": controlToLoad = "Controls/AlbumEdit.ascx"; break;
                        case "4":
                            controlToLoad = "Controls/ImageEdit.ascx";
                            if (ImageId > 0)
                            {
                                controlToLoad = "Controls/EditGalleryImage.ascx";
                            }
                            break;
                        case "5": controlToLoad = "Controls/GalleryViewForHomePage.ascx"; break;
                    }
                }
                else
                {
                    controlToLoad = "Controls/AlbumListView.ascx";
                    if (ImageId > 0)
                    {
                        controlToLoad = "Controls/AlbumImagesView.ascx";
                    }
                }

                var mbl = (GalleryModuleModuleBase)LoadControl(controlToLoad);
                mbl.ModuleConfiguration = ModuleConfiguration;
                mbl.ID = System.IO.Path.GetFileNameWithoutExtension(controlToLoad);
                phViewControl.Controls.Add(mbl);
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        //protected void rptItemListOnItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        //    {
        //        var lnkEdit = e.Item.FindControl("lnkEdit") as HyperLink;
        //        var lnkDelete = e.Item.FindControl("lnkDelete") as LinkButton;

        //        var pnlAdminControls = e.Item.FindControl("pnlAdmin") as Panel;

        //        var t = (Album)e.Item.DataItem;

        //        if (IsEditable && lnkDelete != null && lnkEdit != null && pnlAdminControls != null)
        //        {
        //            pnlAdminControls.Visible = true;
        //            lnkDelete.CommandArgument = t.AlbumId.ToString();
        //            lnkDelete.Enabled = lnkDelete.Visible = lnkEdit.Enabled = lnkEdit.Visible = true;

        //            lnkEdit.NavigateUrl = EditUrl(string.Empty, string.Empty, "Edit", "tid=" + t.AlbumId);

        //            ClientAPI.AddButtonConfirm(lnkDelete, Localization.GetString("ConfirmDelete", LocalResourceFile));
        //        }
        //        else
        //        {
        //            pnlAdminControls.Visible = false;
        //        }
        //    }
        //}

        //public void rptItemListOnItemCommand(object source, RepeaterCommandEventArgs e)
        //{
        //    if (e.CommandName == "Edit")
        //    {
        //        Response.Redirect(EditUrl(string.Empty, string.Empty, "Edit", "tid=" + e.CommandArgument));
        //    }

        //    if (e.CommandName == "Delete")
        //    {
        //        var tc = new GalleryController();
        //        //tc.DeleteItem(Convert.ToInt32(e.CommandArgument), ModuleId);
        //    }
        //    Response.Redirect(DotNetNuke.Common.Globals.NavigateURL());
        //}

        //public ModuleActionCollection ModuleActions
        //{
        //    get
        //    {
        //        var actions = new ModuleActionCollection
        //            {
        //                {
        //                    GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
        //                    EditUrl(), false, SecurityAccessLevel.Edit, true, false
        //                }
        //            };
        //        return actions;
        //    }
        //}
    }
}