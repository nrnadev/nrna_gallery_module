﻿/*
' Copyright (c) 2017  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using DotNetNuke.Entities.Modules;
using NRNA.Modules.GalleryModule.Components;
using System.Text;
using DotNetNuke.Services.FileSystem;

namespace NRNA.Modules.GalleryModule
{
    public class GalleryModuleModuleBase : PortalModuleBase
    {
        public int ImageId
        {
            get
            {
                var qs = Request.QueryString["imgid"];
                if (qs != null)
                    return Convert.ToInt32(qs);
                return -1;
            }

        }

        public int AlbumId
        {
            get
            {
                var qs = Request.QueryString["albid"];
                if (qs != null)
                    return Convert.ToInt32(qs);
                return -1;
            }

        }

        public string GetAlbumLink(string albumid)
        {
            return new GalleryController().GetAlbumLink(TabId, Convert.ToInt32(albumid));
        }

        public string GetAlbumCover(string albumId)
        {
            return new GalleryController().GetAlbumCover(Convert.ToInt32(albumId));
        }

        public string GetAlbumImageFile(string fileId)
        {
            if (fileId == "-1")
            {
                fileId = "3245";
            }
            StringBuilder sb = new StringBuilder("/Portals/");
            try
            {
                IFileInfo fi = FileManager.Instance.GetFile(Convert.ToInt32(fileId));
                sb.Append(fi.PortalId);
                sb.Append("/");
                sb.Append(fi.RelativePath);
            }
            catch (Exception ex)
            {
                return "";
            }
            return sb.ToString();



        }

        public int ModuleDefinitionID
        {
            get
            {
                return new ModuleController().GetModule(ModuleId, TabId, false).ModuleDefID;
            }
        }

        public string GetImageFromImageID(string fileId)
        {
            if (fileId == "-1")
            {
                fileId = "3245";
            }
            string baseUrl = (Request.IsSecureConnection ? "https://" : "http://") + base.PortalAlias.HTTPAlias;
            var image = (DotNetNuke.Services.FileSystem.FileInfo)FileManager.Instance.GetFile(Convert.ToInt32(fileId));
            if (image != null)
            {
                return baseUrl + FileManager.Instance.GetUrl(image);
            }
            else
                return "";
        }

        public string GetThumbNail(string fileid, int height)
        {
            var oFile = FileManager.Instance.GetFile(Convert.ToInt32(fileid));
            //example format: bbimagehandler.ashx? ImageUrl = http://nrna.pathway.com.np/portals/1/1.jpg&width=200
            string test = String.Format("/bbimagehandler.ashx?ImageUrl={0}&height={1}", GetImageFromImageID(fileid), height);
            return test;
        }
    }
}